<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PenyakitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $penyakit = array(
            'Tungro',
            'Kerdil Rumput',
            'Blast',
            'Hawar Pelepah',
            'Hawar Bakteri'
        );

        foreach ($penyakit as $nama) {
	        DB::table('penyakit')->insert([
                'nama' => $nama,
                'penyebab' => 'virus'
            ]);
        }
    }
}
