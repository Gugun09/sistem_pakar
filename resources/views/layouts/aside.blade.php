<div class="col-md-3">
    <div class="panel panel-default">
        <div class="panel-body" align="center">
            <img src="{{ asset('assets/img/padi.png') }}" class="img-rounded img-responsive">
        </div>
    </div>

	@if (Auth::user())
	    <ul class="list-group">
		  	<li class="list-group-item" align="center"><a class="btn btn-primary" href="{{ route('admin.gejala') }}">Gejala</a></li>
		  	<li class="list-group-item" align="center"><a class="btn btn-primary" href="{{ route('admin.penyakit') }}">Penyakit</a></li>
		</ul>
    @endif
</div>
