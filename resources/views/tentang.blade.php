<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
        <x-jet-action-section>
            <x-slot name="title">
                SISTEM PAKAR
            </x-slot>
            <x-slot name="description">
                Tugas Tambahan Pembuatan Sistem Pakar Padi
            </x-slot>
            <x-slot name="content">
                <table class="table table-light">
                    <tbody>
                        <tr>
                            <td>Nama</td>
                            <td>:</td>
                            <td>Faisal</td>
                        </tr>
                        <tr>
                            <td>Kelas</td>
                            <td>:</td>
                            <td>VII C</td>
                        </tr>
                        <tr>
                            <td>Program Studi</td>
                            <td>:</td>
                            <td>Teknologi Rekayasa Perangkat Lunak</td>
                        </tr>
                    </tbody>
                </table>
            </x-slot>
        </x-jet-action-section>
    </x-jet-authentication-card>
</x-guest-layout>
