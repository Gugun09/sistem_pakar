<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Dashboard extends Component
{
    public $pasien;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($pasien)
    {
        $this->pasien = $pasien;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.dashboard');
    }
}
