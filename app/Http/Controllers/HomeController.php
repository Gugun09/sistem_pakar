<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Penyakit;
use App\Models\Gejala;

class HomeController extends Controller
{
    public function index()
    {
        return view('beranda');
    }
    public function penyakit()
    {
        $penyakit = Penyakit::all();
        return view('dashboard.penyakit', compact('penyakit'));
    }

    public function gejala()
    {
        $gejala = Gejala::all();
        return view('dashboard.gejala', compact('gejala'));
    }

    public function detail(Penyakit $penyakit) {
        return view('dashboard.penyakit-show', compact('penyakit'));
    }
}
